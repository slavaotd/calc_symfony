<?php

namespace App\Controller;

use App\Entity\CalcSum;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiCalcController extends AbstractController
{
    /**
     * @Route("/api/calc", name="app_api_calc")
     */
    public function index(): Response
    {
        return $this->json([
            'it' => 'works!'
        ]);
    }

    /**
     * @Route("/api/calc-sum", name="app_api_calc_sum", methods={"POST"})
     */
    public function sum(Request $request): Response
    {
        if ($request->getContentType() !== 'json') {
            return $this->json('Request body must be json', 400);
        }

        $data = json_decode($request->getContent(), true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            return $this->json('Json not valid', 400);
        }

        if (
            !isset($request->toArray()['number1'])
            || !isset($request->toArray()['number2'])
        ) {
            return $this->json('Not enough parameters', 404);
        }

        $calc_sum = new CalcSum($request->toArray()['number1'], $request->toArray()['number2']);

        if (!$calc_sum->is_valid()) {
            return $this->json('Not valid parameters', 400);
        }

        return $this->json([
            'sum' => $calc_sum->calculate()
        ]);
    }
}
