<?php

// src/Entity/CalcSum.php
namespace App\Entity;


/**
 * Class CalcSum
 * @package App\Entity
 */
class CalcSum implements CalcInterface
{
    private $number1 = 0;
    private $number2 = 0;
    private $is_valid;


    /**
     * @param $value1
     * @param $value2
     */
    public function __construct($value1, $value2) {

        $this->is_valid = is_numeric($value1) && is_numeric($value2);
        if (!$this->is_valid) {
            return;
        }

        $this->number1 = $value1;
        $this->number2 = $value2;
    }

    /**
     * Function to sum
     *
     * @return float|integer
     */
    public function calculate()
    {
        return $this->number1 + $this->number2;
    }

    /**
     * Is valid numbers
     *
     * @return bool
     */
    public function is_valid()
    {
        return $this->is_valid;
    }

}