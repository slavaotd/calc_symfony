<?php

// src/Entity/CalcInterface.php
namespace App\Entity;


/**
 * Interface CalcInterface
 * @package App\Entity
 */
interface CalcInterface
{
    /**
     * CalcInterface constructor.
     * @param $value1
     * @param $value2
     */
    public function __construct($value1, $value2);

    /**
     * @return mixed
     */
    public function calculate();

    /**
     * @return mixed
     */
    public function is_valid();
}